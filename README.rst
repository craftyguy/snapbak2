Brief Description
=================
``snapbak2`` is a very simple tarsnap_ frontend, forked from snapbak_.

Main features:

- No config file needed; completely driven by command line
- Do hourly, daily, weekly, monthly backup with N sets per type
- Deletes older archives (older than N sets)

Key (functional) differences from snapbak_ include:

- Parameterized commandline arguments (positional arguments are not great
  when every argument is positional..)

.. _tarsnap: http://tarsnap.com/
.. _snapbak: https://github.com/opencoff/snapbak

Requirements
------------
* Python 3.8.x (on any supported platform)
* Tested on:
    - Arch Linux

Installation
------------
Copy the ``snapbak2`` script to your preferred location in ``$PATH``. I put
this in ``$HOME/bin``.

Usage
-----

      snapbak2 [-h] [-n] [-v] [-c CACHEDIR] [-V] [-k KEYFILE] -p PREFIX -t {hourly,daily,weekly,monthly} [--keep NUM] Dir [Dir ...]

positional arguments:

     **Dir**                  One or more directories to backup

optional arguments:
  -h, --help            show this help message and exit
  -n, --dry-run         Do a dry-run, don't actually commit things [default: False]
  -v, --verbose         Show verbose progress messages [False]
  -c CACHEDIR, --cachedir CACHEDIR         Use specified tarsnap cache directory. [default: $XDG_CACHE_HOME/tarsnap or ~/.cache/tarsnap if $XDG_CACHE_HOME is unset]
  -V, --version         show program's version number and exit
  -k KEYFILE, --keyfile KEYFILE         Tarsnap keyfile [default: /root/tarsnap.key]
  -p PREFIX, --prefix PREFIX         The tarsnap archive prefix. [default: system hostname]

  -t TYPE, --type TYPE         Backup type; one of 'hourly', 'daily', 'weekly', or 'monthly'

  --keep NUM            Number of backups of the specified type to keep. [default: hourly: 24, daily: 10, weekly: 6, monthly: 14]
  -x EXCLUDE, --exclude EXCLUDE
                        Exclude files/dirs that match PATTERN (see tarsnap manpage for info on expected pattern format). This parameter can be specified multiple times.

The archive name in the tarsnap cloud is derived as ``Prefix-Type-Tstamp``; where:

- ``Prefix`` is the archive name prefix from the command line
- ``Type`` is the backup type (daily, weekly etc.)
- ``Tstamp`` is the current date, time in the format YYYY-MMM-DD-hh-mm; where:
  YYYY: four digit year, MMM: short month name, DD: two digit date,
  hh: two digit hour in 24 hour format, mm: two digit minute.


Examples
========
- Backup dirA, dirB identified by 'work' and keep last 7 days worth of
  daily backups::
  
      snapbak2 -k /path/to/Keyfile.key -t daily -p work --keep 7 dirA dirB

- Backup directories *10*, *20* identified by ``photos`` and keep
  last 10 weeks worth of weekly backups::

      snapbak2 -k /path/to/Keyfile.key -t weekly --keep 10 -p photos 10 20


- Do a monthly backup of directories *10*, *20* identified by the prefix ``foo``
  and use the program defaults for number of retained backups::

      snapbak2 -k /path/to/Keyfile.key -p foo -t monthly 10 20


FAQ
===
- How do I know which archives are stored in the cloud?
    ``tarsnap --keyfile /path/to/Keyfile.key --list-archives``

- Why do you need to do hourly backups?
    I don't know. But if you do, ``snapbak2`` supports it.


- How many daily, weekly and monthly sets do I need to keep?
    It depends on your needs. I use the following:

    - 10 daily backups
    - 6 weekly backups
    - 14 monthly backups

    And no, I don't use hourly backups.


.. vim:ft=rst:notextmode:expandtab:tw=74:sw=4:ts=4:
